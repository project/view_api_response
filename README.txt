CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

It provides user interface to view the response of each API in array format.     

More info:

 * For a full description of the module, visit [the project page]
   (https://www.drupal.org/project/view_api_response).  


INSTALLATION
------------

 * Install the View API Response module as you would normally install a 
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 
   for further information.    

CONFIGURATION
-------------
  1) Go to /admin/structure/view-api-response and click "Add API Response Type" button.
  2) Click "Add API" button to add each API details.
  3) Update the necessary details and click the save button.
  4) To view the API response for the each API, Click the Edit link on the "View API Response Type" page
     and click the "View [ID] API Response" button on top of the edit page.

MAINTAINERS
-----------

Current maintainers:
  Elavarasan R - https://www.drupal.org/user/1902634
