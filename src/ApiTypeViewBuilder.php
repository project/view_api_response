<?php

namespace Drupal\view_api_response;

use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Defines API Type view builder.
 */
class ApiTypeViewBuilder extends EntityViewBuilder {}
