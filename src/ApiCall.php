<?php

namespace Drupal\view_api_response;

/**
 * Defines API functionalities. 
 */
class ApiCall {

  /**
   * Defines method to Get Request. 
   */
  public static function getRequest($url, $options, $method) { 
    $client = \Drupal::httpClient();
    $response = $client->request($method, $url, $options);
    $code = $response->getStatusCode();
    $results = $response->getBody()->getContents();
    return $results;
  }
  
}
