<?php
namespace Drupal\view_api_response\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\view_api_response\Entity\ApiTypeInterface;

/**
 * Defines the View API Response entity.
 *
 * @ConfigEntityType(
 *   id = "view_api_response_api_type",
 *   label = @Translation("View API Response Type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\view_api_response\ApiTypeListBuilder",
 *     "form" = {
 *       "default" = "Drupal\view_api_response\Form\ApiTypeForm",
 *       "add" = "Drupal\view_api_response\Form\ApiTypeForm",
 *       "edit" = "Drupal\view_api_response\Form\ApiTypeForm",
 *       "delete" = "Drupal\view_api_response\Form\ApiTypeDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\view_api_response\ApiTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "view_api_response_api_type",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },   
 *   links = {     
 *     "canonical" = "/admin/structure/view-api-response/{view_api_response_api_type}",
 *     "add-form" = "/admin/structure/view-api-response/add",
 *     "edit-form" = "/admin/structure/view-api-response/{view_api_response_api_type}/edit",
 *     "delete-form" = "/admin/structure/view-api-response/{view_api_response_api_type}/delete",
 *     "collection" = "/admin/structure/view-api-response"    
 *   }
 * )
 */
class ApiType extends ConfigEntityBase implements ApiTypeInterface {

  /**
   * The Services ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Services label.
   *
   * @var string
   */
  protected $label;

  // Your specific configuration property get/set methods go here,
  // implementing the interface.
}