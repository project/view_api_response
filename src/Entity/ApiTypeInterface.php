<?php

namespace Drupal\view_api_response\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an API type entity.
 */
interface ApiTypeInterface extends ConfigEntityInterface {
  // Add get/set methods for your configuration properties here.
}
