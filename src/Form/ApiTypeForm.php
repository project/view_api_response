<?php

namespace Drupal\view_api_response\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Core\Url;

/**
 * Form handler for the entity add and edit forms.
 */
class ApiTypeForm extends EntityForm {

  /**
   * Constructs an API Type Form object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $entity = $this->entity;

    if(!empty($entity->id())) {
      $form['import_link'] = [
        '#type' => 'link',
        '#title' => $this->t('View [@id] API Response', ['@id' => ucfirst($entity->id())]),
        '#url' => Url::fromRoute('view_api_response.response', ['type' => $entity->id()]),        
        '#attributes' => [
          'class' => [
            'btn btn-primary button button--primary import-btn',
          ],
        ],
      ];      
    }
   
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#description' => $this->t("Name of the API type."),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$entity->isNew(),
    ];
    $form['server_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Server Url'),
      '#maxlength' => 1000,
      '#default_value' => $entity->server_url,
      '#description' => $this->t("Server Url."),
      '#required' => TRUE,
    ];
    $form['source'] = [
      '#type' => 'select',
      '#title' => $this->t('Source'),
      '#options' => ['json' => 'Json', 'xml' => 'Xml'],
      '#default_value' => $entity->source,
      //'#description' => $this->t("Server Url."),
      '#required' => TRUE,
      //'#weight' => 20,
    ];
    $form['method'] = [
      '#type' => 'select',
      '#title' => $this->t('Method'),
      '#options' => ['GET' => 'GET', 'POST' => 'POST'],
      '#default_value' => $entity->method,     
      '#required' => TRUE,      
    ];
    $form['header'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Header'),
      '#default_value' => $entity->header,      
      '#description' => $this->t('Mention header, if require. Enter multiple fields in the next line. Example : key|value'),
      '#weight' => '20',      
    ];
    $form['proxy_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Proxy Status'),        
      '#weight' => 10,
      '#default_value' => $entity->proxy_status,
    ];
    $form['proxy'] = [
      '#title' => $this->t('Proxy'),
      '#type' => 'textfield',
      '#default_value' => $entity->proxy,
      '#states' => [
        'visible' => [
          ':input[name="proxy_status"]' => ['checked' => TRUE],
        ],
      ],
      '#weight' => 10,
    ];    
    $form['auth_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Authentication'),        
      '#weight' => 10,
      '#default_value' => $entity->auth_status,
    ];
    $form['username'] = [
      '#title' => $this->t('User Name'),
      '#type' => 'textfield',
      '#default_value' => $entity->username,
      '#states' => [
        'visible' => [
          ':input[name="auth_status"]' => ['checked' => TRUE],
        ],
      ],
      '#weight' => 20,
    ];
    $form['password'] = [
      '#title' => $this->t('Password'),
      '#type' => 'textfield',
      '#default_value' => $entity->password,
      '#states' => [
        'visible' => [
        ':input[name="auth_status"]' => ['checked' => TRUE],
        ],
      ],
      '#weight' => 20,
    ];        
    
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;    
    $status = $entity->save();

    if ($status === SAVED_NEW) {
      $this->messenger()->addMessage($this->t('The %label entity created.', [
        '%label' => $entity->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label entity updated.', [
        '%label' => $entity->label(),
      ]));
    }

    $form_state->setRedirect('entity.view_api_response_api_type.collection');
  }

  /**
   * Helper function to check whether a configuration entity exists.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('view_api_response_api_type')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

  /**
   * Get content type list.
   */
  public function getContentTypeList() {
    $types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    foreach($types as $key => $value) {
      $content_type[$key] = $value->get('name');      
    }
    return $content_type;
  }

  /**
   * Get Vocabulary List.
   */
  public function getVocabularyList() {
    $vocabularies = Vocabulary::loadMultiple();
    $vocabulariesList = [];
    foreach ($vocabularies as $vid => $vocablary) {
      $vocabulariesList[$vid] = $vocablary->get('name');
    }
    return $vocabulariesList;
  }

}