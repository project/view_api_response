<?php

namespace Drupal\view_api_response\Controller;

use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\Exception\RequestException;
use Drupal\Component\Serialization\Json;
use Drupal\view_api_response\ApiCall;



/**
 * View API Response Controller.
 */
class ViewApiResponseController extends ControllerBase {

  /**
   * View API Response Controller.
   */
 
  public function getResponse() {  
    $type = \Drupal::request()->query->get('type');
    $results = [];
    $pageTitle = '';
    if(!empty($type)) {  
      $pageTitle = $this->t('Response for @title API', ['@title' => ucfirst($type)]);
      $entity = \Drupal::entityTypeManager()->getStorage('view_api_response_api_type')->loadByProperties(['id' => $type]);
      $entity = reset($entity);
      $url = $entity->get('server_url'); 
      $proxy_status = $entity->get('proxy_status');
      $proxy = $entity->get('proxy');
      $enable_status = $entity->get('enable_status'); 
      $source = $entity->get('source');
      $method = $entity->get('method');   
      $auth_status = $entity->get('auth_status');
      $str_header = $entity->get('header');
      $options = [];
      if($str_header != '') {
        $arr_header = [];
        $arr_header = explode("\n", $str_header);
        foreach($arr_header as $value) {
          if($value != '') {
            $header = explode("|", $value);          
            $options['headers'][trim($header[0])] = trim($header[1]);
          }                     
        }
      }  
      if($auth_status) {
        $username = $entity->get('username');
        $password = $entity->get('password');
        $options['auth'] = Json::encode([$entity->get('username'), $password]);
      }   
      if($proxy_status) {
        $options['proxy'] = $proxy;
      } 
      if($source === 'json') {
        $options['headers']['Content-Type'] = 'application/json';
        $results = ApiCall::getRequest($url, $options, $method);
        $results = Json::decode($results);
      }
      else if($source === 'xml') {  
        $options['headers']['Content-Type'] = 'application/xml';    
        $xml = ApiCall::getRequest($url, $options, $method);
        $xml_obj = simplexml_load_string($xml);
        $json = Json::encode($xml_obj); 
        $results = Json::decode($json, true); 
      }
    }
    return [
      '#title' => $pageTitle,
      '#markup' => '<pre><code>' . print_r($results, TRUE) . '</code></pre>'
    ];
  }

}